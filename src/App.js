import React from 'react';
import './App.css';
import {BrowserRouter, withRouter} from 'react-router-dom';
import HeaderContainer from "./components/Header/HeaderContainer";
import {connect, Provider} from "react-redux";
import {compose} from "redux";
import store from './components/redux/redux-store';
import SitesListContainer from "./components/SitesList/SitesListContainer";

class App extends React.Component {
  catchAllUnhandledErrors = (reason, promise) => {
    //alert("Some error occured");
    //console.error(promiseRejectionEvent);
  }

  componentDidMount() {
    window.addEventListener("unhandledrejection", this.catchAllUnhandledErrors);
  }

  componentWillUnmount() {
    window.removeEventListener("unhandledrejection", this.catchAllUnhandledErrors);
  }

  render() {
    return (
        <div className='app-wrapper'>
          <HeaderContainer/>
          <SitesListContainer/>
        </div>
    );
  }
}

const mapStateToProps = (state) => ({
})

let AppContainer = compose(
    withRouter,
    connect(mapStateToProps))(App);

const PingatorWebApp = (props) => {
  //return <BrowserRouter basename={process.env.PUBLIC_URL}>
  //HashRouter - старый компонент, который решает подгрузку правильной папки с index.html с github
  return <BrowserRouter>
    <Provider store={store}>
      <React.StrictMode>
        <AppContainer/>
      </React.StrictMode>
    </Provider>
  </BrowserRouter>
};

export default PingatorWebApp;
