import * as axios from "axios";

export const usersAPI = {
    pingSite(siteUrl = '') {

        const instance = axios.create({
            //withCredentials: true,
            headers: {
                //'Access-Control-Allow-Origin': '<origin>',
                'content-type': 'application/json'
            }
        });

        return instance.get(siteUrl);
    },
}

export const getPingSite = (siteUrl) => async() => {
    await usersAPI.pingSite(siteUrl);
}