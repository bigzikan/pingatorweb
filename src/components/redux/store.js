import sitesListReducer from "./sitesList-reducer";

let store = {
    _state: {
        sitesList: {
            columns: [
                {key: 'id', name: 'ID'},
                {key: 'site', name: 'Сайт'},
                {key: 'lastcall', name: 'Последний вызов'},
                {key: 'interval', name: 'Интервал (сек)'},
                {key: 'coderesponse', name: 'Код ответа'},
                {key: 'message', name: 'Сообщение'},
            ],
            rows: [
            //    {site: '', lastcall: Date(0), interval: 0, coderesponse: 0, message: ''},
            ],
            newSite: {
                site: '',
                interval: 0,
            },
            currentItem: 0,
        },
    },
    _callSubscriber() {
        console.log('state is changed');
    },

    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._callSubscriber = observer;
    },

    dispatch(action) {
        this._state.sitesList = sitesListReducer(this._state.sitesList, action);

        this._callSubscriber(this._state);
    }
};


export default store;
window.store = store;