import {updateObjectInArray} from "../../utils/object-helpers";
import {usersAPI, getPingSite} from '../api/api';

const ADD_SITE = 'ADD_SITE';
const EDIT_SITE = 'EDIT_SITE';
const SET_CURRENT_ITEM = 'SET_CURRENT_ITEM';
const SET_PING_STATUS = 'SET_PING_STATUS';

let initialState = {
    columns: [
        {key: 'id', name: 'ID'},
        {key: 'site', name: 'Сайт'},
        {key: 'lastcall', name: 'Последний вызов'},
        {key: 'interval', name: 'Интервал (сек)'},
        {key: 'coderesponse', name: 'Код ответа'},
        {key: 'message', name: 'Сообщение'},
    ],
    rows: [
        //  {id: 0, site: '', lastcall: Date(0), interval: 0, coderesponse: 0, message: ''},
    ],
    newSite: {
        site: '',
        interval: 0,
    },
    currentItem: 0,
};

const sitesListReducer = (state = initialState, action) => {
    // let {coderesponse, message} = await usersAPI.pingSite(site);
    switch (action.type) {
        case ADD_SITE:
            let {site, interval} = action.newSite;
            let id = state.rows.length;
            return {
                ...state,
                rows: [...state.rows,
                    {
                        id: id, site: site, lastcall: Date(0), interval: interval,
                        coderesponse: 0, message: ''
                    }]
            };
        case EDIT_SITE:
            return {
                ...state,
                rows: updateObjectInArray(state.rows, action.editingSite.id, "id",
                    {
                        site: action.editingSite.site,
                        interval: action.editingSite.interval,
                        lastcall: Date(0),
                        coderesponse: 0, message: ''
                    })
            }
        case SET_PING_STATUS:
            return {
                ...state,
                rows: updateObjectInArray(state.rows, action.editingSite.id, "id",
                    {
                        coderesponse: action.editingSite.coderesponse,
                        message: action.editingSite.message
                    })
            }
        case SET_CURRENT_ITEM:
            return {
                ...state,
                currentItem: action.currentItem,
            };
        default:
            return state;
    }
}

export const addSite = (newSite) => ({
    type: ADD_SITE, newSite
});

export const setCurrentItem = (currentItem) => ({
    type: SET_CURRENT_ITEM, currentItem
});

export const editSite = (editingSite) => ({
    type: EDIT_SITE, editingSite
});

export const setPingStatus = (editingSite) => ({
    type: SET_PING_STATUS, editingSite
});

export const getAuthUserData = (id, site) => async (dispatch) => {
    let {coderesponse, message} = await usersAPI.pingSite(site).then(response => {
        return {coderesponse: response.status, message: response.statusText};
    })
        .catch(function (error) {
            return {coderesponse: '-1', message: error.stack};
        })
    dispatch(setPingStatus({
        id: id,
        coderesponse: coderesponse,
        message: message
    }));
}

export const pingAllSites = () => (dispatch, getState) => {
    const rows = getState().sitesList.rows;

    rows.forEach(element => {
        let promise = dispatch(getAuthUserData(element.id, element.site));
        Promise.all([promise]).then(() => {
        });
        }
    );
}

export default sitesListReducer;