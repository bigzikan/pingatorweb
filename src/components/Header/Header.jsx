import React, {useState} from 'react';
import classes from './Header.module.css';
import logo from './../../logo.svg';
import ButtonModalWindow from "../ModalWindow/ButtonModalWindow";
import {Button} from "react-bootstrap";

const Header = (props) => {

   return (
        <header className={classes.header}>
            <img src={logo}></img>
            <div className={classes.floating}>
                <ButtonModalWindow headerText='Добавить сайт' submitText='Добавить' isEditMode={false} id={props.currentItem}/>
                <ButtonModalWindow headerText='Редактировать' submitText='Редактировать' isEditMode={true} id={props.currentItem}/>
                <Button onClick={props.pingAllSites}>Пинг всего листа</Button>
            </div>
        </header>)
}

export default Header;