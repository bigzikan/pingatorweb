import React from 'react';
import Header from "./Header";
import {connect} from 'react-redux';
import {pingAllSites} from './../redux/sitesList-reducer'

class HeaderContainer extends React.Component {
    render() {
        return (
            <Header {...this.props}/>
        )
    }
};

const mapStateToProps = (state) => ({
    currentItem: state.sitesList.currentItem
});

const mapDispatchToProps = (dispatch) => {
    return {
        pingAllSites: () => {
            dispatch(pingAllSites());
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);