import React, {useState} from 'react';
import Modal from 'react-modal';
import classes from './ButtonModalWindow.module.css';
import {addSite, editSite} from "../redux/sitesList-reducer";
import {Field, reduxForm} from "redux-form";
import {createField, Input} from '../common/FormsControls/FormsControls';
import {required} from "../../utils/validators/validator";
import {connect} from "react-redux";

const ButtonModalWindow = (props) => {

    // Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
    Modal.setAppElement('#root');

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            width: '250px'
        }
    };

    let subtitle;
// eslint-disable-next-line react-hooks/rules-of-hooks
    const [modalIsOpen, setIsOpen] = useState(false);

    function openModal() {
        setIsOpen(true);
    }

    function afterOpenModal() {
        // references are now sync'd and can be accessed.
        subtitle.style.color = '#000000';
    }

    function closeModal() {
        setIsOpen(false);
    }

    const onSubmit = (formData) => {
        props.isEditMode ?
            props.editSite({site: formData.site, interval: formData.interval, id: props.id}) :
            props.addSite({site: formData.site, interval: formData.interval});
        closeModal();
    }

    const SiteInfoForm = ({handleSubmit, error, submitText}) => {
        return (
            <form onSubmit={handleSubmit}>
                {createField("Сайт", "site", [required], Input)}
                {createField("Интервал (сек)", "interval", [required], Input)}
                <button>{submitText}</button>
                <a> </a>
                <button onClick={closeModal}>Отмена</button>
            </form>
        )
    }

    const SiteReduxForm = reduxForm({form: 'siteInfo'})(SiteInfoForm)

    return (
        <>
            <button className={classes.moduleButton} onClick={openModal}>{props.headerText}</button>
            <Modal
                isOpen={modalIsOpen}
                onAfterOpen={afterOpenModal}
                onRequestClose={closeModal}
                style={customStyles}
                contentLabel="Example Modal"
            >
                <h3 ref={_subtitle => (subtitle = _subtitle)}>{props.headerText}</h3>
                <SiteReduxForm onSubmit={onSubmit} submitText={props.submitText}/>
            </Modal>
        </>
    )
}
const mapStateToProps = (state) => (
    {
        columns: state.sitesList.columns,
        rows: state.sitesList.rows,
        currentItem: state.sitesList.currentItem,
    }
)

const mapDispatchToProps = (dispatch) => {
    return {
        addSite: (newSite) => {
            dispatch(addSite(newSite));
        },
        editSite: (editingSite) => {
            dispatch(editSite(editingSite));
        },
    }
}

//export default ButtonModalWindow;
export default connect(mapStateToProps, mapDispatchToProps)(ButtonModalWindow);