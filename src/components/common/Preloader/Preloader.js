import preloaderGif from "../../../assets/images/preloader.gif";
import classes from "../../Users/Users.module.css";
import React from "react";

let Preloader = (props) => {
    return (
        <div>
            <img src={preloaderGif} className={classes.userPhoto}/>
        </div>
    );
}
export default Preloader;