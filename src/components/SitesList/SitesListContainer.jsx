import React from 'react';
import SitesList from "./SitesList";
import {connect} from 'react-redux';
import {addSite, setCurrentItem} from "../redux/sitesList-reducer";

const emptyTable = {
    columns: [
        {key: 'id', name: 'ID'},
        {key: 'site', name: 'Сайт'},
        {key: 'lastcall', name: 'Последний вызов'},
        {key: 'interval', name: 'Интервал (сек)'},
        {key: 'coderesponse', name: 'Код ответа'},
        {key: 'message', name: 'Сообщение'},
    ],
    rows: [
    //    {site: '', lastcall: Date(0), interval: 0, coderesponse: 0, message: ''},
    ]
}

let table = {};

class SitesListContainer extends React.Component {
    render() {
        table = {...this.props};
        if(!table)
        {
            table = {...emptyTable};
        }
        if (!table.columns) {
            table.columns = emptyTable.columns;
        }
        if (!table.rows) {
            table.rows = emptyTable.rows;
        }
        return (
            <SitesList {...table}/>
        )
    }
};

const mapStateToProps = (state) => ({
    columns: state.sitesList.columns,
    rows: state.sitesList.rows,
    setCurrentItem: setCurrentItem,
});

const mapDispatchToProps = (dispatch) => {
    return {
        addSite: (newSite) => {
            dispatch(addSite(newSite));
        },
        setCurrentItem: (currentItem) => {
          dispatch(setCurrentItem(currentItem));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SitesListContainer);