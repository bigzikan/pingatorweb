import DataGrid from 'react-data-grid';
import classes from './SitesList.module.css';
import React from "react";

const SitesList = (props) => {
    const setCurrentItem = (e) => {
        props.setCurrentItem(e);
    };

    return (
        <DataGrid
            className={classes.datagrid}
            columns={props.columns}
            rows={props.rows}
            onRowClick = {(e)=>{
                setCurrentItem(e)}}
        />
    );
};

export default SitesList;